﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CleanEarth.Models;

namespace CleanEarth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : Controller  
    {
        private readonly CEPortalDEVDBContext _context;

        public StoresController(CEPortalDEVDBContext context)
        {
            _context = context;
        }

        // GET: api/Stores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Store>>> GetStores()
        
        {
            //Return all the store data
            var stores = _context.Stores.AsQueryable();

            return await stores.ToListAsync();
        }

        // GET: api/Stores/5
        //[HttpGet("{id}")]
        //public ActionResult<Store> GetStore(string id)
        //{
        //    //var store = await _context.Stores.FindAsync(id.ToString());
        //    var stores = _context.Stores.AsQueryable();
        //    var GetStoreID = _context.GetStoreId(id);
        //    return GetStoreID;
        //}

        // Get Store by Id
        [HttpGet("{StoreId}")]
        public async Task<IActionResult> GetStoreId(string StoreId)
        {
            List<Store> _res = await Task.Run(() => _context.getStores());
            List<Store> filter_stores = _res; // load all first already
            List<Store> StoreIds = new List<Store>();
            if (StoreId != null)
            {
                StoreIds = filter_stores.FindAll(x => x.StoreName == StoreId);
                filter_stores = StoreIds;
            }
            return Ok(filter_stores);
        }

        // PUT: api/Stores/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStore(int id, Store store)
        {
            if (id != store.StoreNumber)
            {
                return BadRequest();
            }

            _context.Entry(store).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Stores
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Store>> PostStore(Store store)
        {
            _context.Stores.Add(store);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStore", new { id = store.StoreNumber }, store);
        }

        // DELETE: api/Stores/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStore(int id)
        {
            var store = await _context.Stores.FindAsync(id);
            if (store == null)
            {
                return NotFound();
            }

            _context.Stores.Remove(store);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StoreExists(int id)
        {
            return _context.Stores.Any(e => e.StoreNumber == id);
        }
    }
}
