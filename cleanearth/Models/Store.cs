﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CleanEarth.Models
{
    public partial class Store
    {
        public string Division { get; set; }
        public string Region { get; set; }
        public int StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
