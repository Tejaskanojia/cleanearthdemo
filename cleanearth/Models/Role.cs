﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CleanEarth.Models
{
    public partial class Role
    {
        public Role()
        {
            Users = new HashSet<User>();
        }

        public string Type { get; set; }
        public string Description { get; set; }
        public string CustomAttribute { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
