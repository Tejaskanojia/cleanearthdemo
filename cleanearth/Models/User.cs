﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CleanEarth.Models
{
    public partial class User
    {
        public long Id { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime LastLogin { get; set; }
        public string CustomAttribute { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Role { get; set; }

        public virtual Role RoleNavigation { get; set; }
    }
}
